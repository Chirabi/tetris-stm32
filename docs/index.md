# Projet Tetris sur carte STM32 discovery

Les fichiers du projet sont disponibles à l'adresse :
[https://gitlab.com/Chirabi/tetris-stm32](https://gitlab.com/Chirabi/tetris-stm32)


Le projet contient :  

+ Un fichier main qui contient le programme en C du jeu Tetris. On utilisera le fichier mainV11 qui est la dernière version du jeu Tetris.

+ Un fichier .ioc qui est adapté à la une carte STM32F746G-DISCO avec l'extension de l'ENS Paris Saclay "vieille" de la carte.

![Image1](./img/image1.jpg)
*Photo de la carte avec le jeu lancé*

