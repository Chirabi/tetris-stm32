# Programme 

Le programme élaboré contient 5 tâches : Objet, myChrono, Tableau, Affichage, Perdu

Les variables globales sont :

+ int Xpos = 13 , Ypos = 5 , Xpos_prec = 0 , Ypos_prec = 0; Permet de récupérer la  nouvelle position et la position précédent de l'objet

+ char Tableau[15][10]; Création d'un tableau où les objets vont se "déplacer"

+ int objet = 0; Permet de savoir qu'elle objet on utilise / Affiche
+ int objet_suivant = 6; Permet de savoir l'objet suivant qu'on utilisera

+ int rotation = 0 , rotation_prec = 0; Permet de connaitre la rotation de l'objet et la rotation precédente de l'objet
+ int nouveau=0; Permet la création d'un nouvel objet lorsque le précédent répond aux critères d'arrêt
+ int chrono = 0; Permet la gestion du temps. Actuellement, le temps est fixé
+ int score = 0; S'incrémente de 1 pour chaque ligne détruite
+ int perdu=0; S'active quand un objet dépasse la 12ème ligne
+ int best = 0; Garde en mémoire le meilleur score des parties

## Les taches 
### Tâche objet
La tache objet contient tous les objets présents dans le jeu numérotés de 0 à 6 (Carre =  0, Barre = 1, T = 2, L = 3, Lmiroir = 4, Z = 5, Zmiroir = 6).

Cette tache contient les déplacements à effectuer lorsqu'il recoit un message venant du clavier, les rotations et les conditions limites / bords.

### Tâche tableau
La tache tableau correspond au quadrillage des positions que l'objet peut avoir. C'est un tableau de 15 x 10. Les X correspond au sens de la descente.

Lorsque l'objet est en mouvement, on lui attribue dans le tableau un chiffre 1,3,4,5,6,7,8 en fonction de l'objet actif.

Lorsque qu'une condition d'arrêt est vérifié, on fige l'objet et on remplace dans le tableau aux positions de l'objet par le chiffre 2.
Par la suite, une fonction verification est lancée pour regarder si des lignes peuvent être supprimées, regarder si la condition perdu est atteinte, d'attribuer un nouvel objet aléatoirement, d'incrémenter le score si des lignes sont supprimés.

### Tâche affichage
La tache affichage permet l'affichage du Tableau sur l'écran LCD présent sur la carte. Chaque case du tableau corresponde à un carré de 20 x 20 pixels. Lorsqu'on lit la valeur 1, on affiche un carré jaune, la valeur 2 un carré noir, la valeur 3 un carré bleu cyan etc.... (Permet de suivre les bonnes couleurs comme dans le vrai jeu TETRIS :) sauf pour le noir qui affiche les objets figés)

### Tâche chrono
La tâche chrono permet la descente d'un objet périodiquement. Elle peut être modifié manuellement dans le code pour gérer la vitesse de descente. Elle affiche également le score en cours.

### Tâche perdu
La tache perdu permet d'afficher l'écran game over, d'afficher le score actuel et le meilleur score, de suspendre les tâches et de redémarrer les taches si l'utilisateur appuie sur le touche "k".

## Illustrations
+ Un exemple de tableau pour l'objet carré avec un objet figé :

![illustration](./img/illustration.png)
*Illustration du tableau et de l'affichage*

+ L'écran est utilisé ainsi pendant une partie :
![illustration2](./img/illustration_2.png)
*Schéma de l'utilisation de l'écran*

+ Lorsque la partie est perdue, on a l'affichage d'un nouveau "menu" qui est le suivant : 
![score](./img/score.png)

*Schéma affichage du menu game over*

## Les difficultés rencontrées 

+ Un premier problème qui s'est posé est la gestion des tâches. En effet, aurait été t-il plus judicieux de créer des tâches pour chaque objet? 

+ Un deuxième problème qui s'est posé est l'affichage des objets dans le tableau. En effet, lorsque nous créons un nouvel objet, il s'affiche en haut de l'écran. Cependant, en prenant la même position pour tous les objets, certains pouvaient être "hors de l'écran". Cela a ammené à des bugs voir des crashs du jeu. 

+ Une troisième difficulté a été la gestion du score. En effet, comment savoir si la ligne du tableau était complétée ou non?

+ Un quatrième problème a été la gestion de l'affichage. Avoir l'affichage du score et du jeu en même temps. 

+ Un cinquième problème a été la gestion de la rotation des objets. 

+ Comment gérer le déplacement des objets?

## Les solutions développées

+ La solution retenue afin de résoudre le premier problème est la création d'une tâche pour tous les objets. Cela a permis d'économiser l'utilisation du processeur et a permis d'avoir une compréhension plus facile. Cette tâche regroupe les objets ainsi que les rotations. Il y a également les différentes conditions de bords des différents objets pour éviter qu'ils sortent de l'écran.

+ Pour résoudre le deuxieème problème, chaque objet à un endroit de "spawn" différent pour contourner ce problème.

 + La solution retenue est qu'à chaque fois qu'un objet était figé, on lance une fonction (void verification) qui regarde si les lignes sont complétées ou non. Comme un objet figé devient un 2 dans les tableaux, il suffit de sommer chaque case de la ligne du tableau est vérifié si on obtient le bon nombre. 
Par ailleurs, on regarde les 4 lignes au dessus car la suppresion maximale des lignes est de 4 avec la barre verticale.

+ Pour gérer les différents affichages, on utilise un mutex qui permet d'éviter des couleurs qui se modifient au cours du temps pour l'écriture du texte "Score".

+ Pour la gestion de la rotation des objets, j'ai choisi un point ou l'objet tourne. Au lieu d'utiliser une matrice de rotation pour chaque objet, j'ai codé les différentes rotations de chaque objets ainsi que les différentes conditions pour la rotation en utilisant le schéma suivant :

![score](./img/tetris_pieces2.png)

*Schéma des rotation des pièces avec le point blanc qui représente le pixel autour duquel tourne la pièce*

+ Pour gérer le déplacement des objets, on utilise une liaison série avec l'ordinateur. On utilise une interruption pour récupérer le code de la touche appuyé par l'utilisateur. Le code d'une touche correspond à un déplacement. 

## Le schéma des interactions entre les tâches
![score](./img/image_2021-04-18_171107.png)

*Schema des interactions entre les tâches*

