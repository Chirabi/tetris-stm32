# Guide d'utilisation

Pour jouer sur votre carte, il suffit de téléverser le fichier main v11 et de configurer les ports à l'aide du fichier .ioc

Les règles du jeu sont simples : faire le plus haut score
Chaque ligne complétée augmente le score de 1.
Si un objet est placé dépasse la ligne 12, le jeu s'arrête et l'écran affiche le score actuel et le plus haut score.

Pour déplacer l'objet, il faut d'abord configurer le port série :
+ Vitesse = 115200, Données 8bit, Parité NONE, Stop bits 1, Controle de flux None

Ensuite, téléverser le programme dans votre carte et jouer directement avec les touches 'Q' pour se déplacer vers la gauche d'un bloc, 'S' pour descendre d'un bloc, 'D' pour se déplacer vers la droite d'un bloc, 'A' pour une rotation de l'objet de -90° et 'E' pour une rotation de l'objet de +90°.

Les objets présents sont donnés ci-dessous :


![Image1](./img/tetris_pieces.png)

*Objets implémenter avec les rotations suivantes*