# Amélioration

Ce projet n'est pas totalement abouti. Il reste encore des choses qui peuvent être perfectible. 

En effet, la fonction random ne marche pas très bien car nous avons souvent la même série d'objets.

Un autre point qui peut être améliorer est l'ajout d'une touche qui permet d'échanger avec l'objet actuel avec l'objet suivant.

On pourrait améliorer le visuel du score ainsi que les objets en mettant des contours noirs autour de chaque "carré" de l'objet.

On pourrait avoir aussi une période du chrono qui diminue a chaque fois que le score augmente

On pourrait aussi rajouter un son qui est joué pendant la partie
