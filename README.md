Dernière update est la main v11 avec le .ioc qui va avec dans le dossier ioc.

Pour jouer : Rien de plus simple, il suffit de paramétrer le port série avec la carte avec comme paramètre Vitesse = 115200, Données 8bit, Parité NONE, Stop bits 1, Controle de flux None.

Ensuite, téléverser le programme dans votre carte et jouer directement avec les touches Q pour se déplacer vers la gauche d'un bloc, S pour descendre d'un bloc, D pour se déplacer vers la droite d'un bloc, A pour une rotation de l'objet de -90° et E pour une rotation de l'objet de +90°.

Le game over est atteint lorsque un bloc d'objet atteint la 12ème ligne.

Dans cette version, on a 5 tâches : Objet, myChrono, Tableau, Affichage, Perdu

Les variables globales sont :

int Xpos = 13 , Ypos = 5 , Xpos_prec = 0 , Ypos_prec = 0; Permet de récupérer la  nouvelle position et la position précédent de l'objet
char Tableau[15][10]; Création d'un tableau où les objets vont se "déplacer"
int objet = 0; Permet de savoir qu'elle objet on utilise / Affiche
int objet_suivant = 6; Permet de savoir l'objet suivant qu'on utilisera

int rotation = 0 , rotation_prec = 0; Permet de connaitre la rotation de l'objet et la rotation precédente de l'objet
int nouveau=0; Permet la création d'un nouvel objet lorsque le précédent répond aux critères d'arrêt
int chrono = 0; Permet la gestion du temps. Actuellement, le temps est fixé

int score = 0; S'incrémente de 1 pour chaque ligne détruite
int perdu=0; S'active quand un objet dépasse la 12ème ligne
int best = 0; Garde en mémoire le meilleur score des parties


La tache objet contient tous les objets présents dans le jeu numérotés de 0 à 6 (Carre =  0, Barre = 1 
T = 2, L = 3, Lmiroir = 4, Z = 5, Zmiroir = 6)
Cette tache contient les déplacements à effectuer lorsqu'il recoit un message venant du clavier, les rotations et les conditions limites / bords

La tache tableau correspond au quadrillage des positions que l'objet peut avoir. C'est un tableau de 15 x 10. Les X correspond au sens de la descente.
Lorsque l'objet est en mouvement, on lui attribue dans le tableau un chiffre 1,3,4,5,6,7,8 en fonction de l'objet actif. 
Lorsque qu'une condition d'arrêt est vérifié, on fige l'objet et on remplace dans le tableau aux positions de l'objet par le chiffre 2. 
Par la suite, une fonction verification est lancée pour regarder si des lignes peuvent être supprimées, regarder si la condition perdu est atteinte, d'attribuer un nouvel objet aléatoirement, d'incrémenter le score si des lignes sont supprimés.

La tache affichage permet l'affichage du Tableau sur l'écran LCD présent sur la carte. Chaque case du tableau corresponde à un carré de 20 x 20 pixels. Lorsqu'on lit la valeur 1, on affiche un carré jaune, la valeur 2 un carré noir, la valeur 3 un carré bleu cyan etc.... (Permet de suivre les bonnes couleurs comme dans le vrai jeu TETRIS :) sauf pour le noir qui affiche les objets figés)

La tâche chrono permet la descente d'un objet périodiquement. Elle peut être modifié manuellement dans le code pour gérer la vitesse de descente. Elle affiche également le score en cours.

La tache perdu permet d'afficher l'écran game over, d'afficher le score actuel et le meilleur score, de suspendre les tâches et de redémarrer les taches si l'utilisateur appuie sur le touche "k".




